import React, { Component } from "react";
import { connect } from "react-redux";
import { Router, Switch, Route, Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import NewJobOffer from "./components/new-joboffer.component";
import JobOfferPage from "./components/joboffer-page.component";
import MyCandidacies from "./components/my-candidacies.component";
import MyJobOffers from "./components/my-joboffers.component";
import CandidaciesForJobOffer from "./components/candidacies-for-joboffer.component";


import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";

import { history } from './helpers/history';

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    };

    history.listen((location) => {
      props.dispatch(clearMessage()); // clear message when changing location
    });
  }

  componentDidMount() {
    const user = this.props.user;

    if (user) {
      this.setState({
        currentUser: user,
        showCandidateActions: user.data.role === "Candidate",
        showRecruiterActions: user.data.role === "Recruiter",
      });
    }
  }

  logOut() {
    this.props.dispatch(logout());
  }

  render() {
    const { currentUser, showCandidateActions, showRecruiterActions } = this.state;

    return (
        <Router history={history}>
          <div>
            <nav className="navbar navbar-expand navbar-dark bg-dark">
              <Link to={"/"} className="navbar-brand">
                jobAgency
              </Link>
              <div className="navbar-nav mr-auto">
                {showCandidateActions && (
                    <li className="nav-item">
                      <Link to={"/"} className="nav-link">
                        Accueil
                      </Link>
                    </li>
                )}

                {showCandidateActions && (
                    <li className="nav-item">
                      <Link to={"/candidacies"} className="nav-link">
                        Mes candidatures
                      </Link>
                    </li>
                )}

                {showRecruiterActions && (
                    <li className="nav-item">
                      <Link to={"/joboffer/new"} className="nav-link">
                        Publier une offre
                      </Link>
                    </li>
                )}
                {showRecruiterActions && (
                    <li className="nav-item">
                      <Link to={"/my-joboffers"} className="nav-link">
                        Mes offres
                      </Link>
                    </li>
                )}
              </div>

              {currentUser ? (
                  <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <Link to={"/profile"} className="nav-link">
                        {currentUser.data.name}
                      </Link>
                    </li>
                    <li className="nav-item">
                      <a href="/login" className="nav-link" onClick={this.logOut}>
                        Déconnexion
                      </a>
                    </li>
                  </div>
              ) : (
                  <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <Link to={"/login"} className="nav-link">
                        Connexion
                      </Link>
                    </li>

                    <li className="nav-item">
                      <Link to={"/register"} className="nav-link">
                        Inscription
                      </Link>
                    </li>
                  </div>
              )}
            </nav>

            <div className="container mt-3">
              <Switch>
                <Route exact path={["/", "/home"]} component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/joboffer/new" component={NewJobOffer} />
                <Route exact path="/joboffer/:id" component={JobOfferPage} />
                <Route exact path="/candidacies" component={MyCandidacies} />
                <Route exact path="/candidacies/:id" component={CandidaciesForJobOffer} />
                <Route exact path="/my-joboffers" component={MyJobOffers} />
              </Switch>
            </div>
          </div>
        </Router>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(App);
