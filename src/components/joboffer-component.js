import React, { Component } from "react";
import { BsDot } from "react-icons/bs"
import "../css/joboffer.css";
import {Redirect} from "react-router-dom";


export default class JobOfferComponent extends Component {
    constructor(props) {
        super(props);
        this.clickJobOffer = this.clickJobOffer.bind(this);

        this.state = {
            content: "",
            isUser: (!!localStorage.getItem('user')),
            redirect: null
        };
    }

    clickJobOffer(jobOfferId, expDate) {
        const today = new Date();
        const expiDate = new Date(expDate);

        if (expiDate < today){
        } else {
            if (window.location.href === "http://localhost:8081/my-joboffers") {
                this.setState({ redirect: "/candidacies/" + jobOfferId });
            } else {
                if (this.state.isUser) {
                    this.setState({ redirect: "/joboffer/" + jobOfferId });
                } else {
                    this.setState({ redirect: "/login" });
                }
            }
        }

    }

    render() {
        const today = new Date();
        const expDate = new Date(this.props.jobOffer.expiration_date);
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        return (
            <div className={ expDate < today ? "mainCard card noPointer" : "mainCard card withPointer"} onClick={ () => this.clickJobOffer(this.props.jobOffer.id, this.props.jobOffer.expiration_date) }>
                <h5 className="card-title mb-2 title">{this.props.jobOffer.title}</h5>
                <h6 className="card-subtitle mb-2 company">{this.props.jobOffer.company}</h6>
                <h6 className="card-subtitle mb-1 city">{this.props.jobOffer.city}<BsDot/>{this.props.jobOffer.contract_type}</h6>
                <h6 className="card-subtitle salary">{this.props.jobOffer.salary}€ par an (estimation)</h6>
                <p className="card-text">{this.props.jobOffer.description.substr(0, this.props.descLenght)}...</p>
            </div>
        )
    }
}
