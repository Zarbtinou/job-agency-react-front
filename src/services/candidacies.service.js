import axios from "axios";

const API_URL = process.env.REACT_APP_BACKEND_URI;

class CandidaciesService {
    getCandidaciesForJobOffer(id) {
        return axios.get(API_URL + "candidacies/joboffer-candidacies/" + id);
    }
    handleAcceptCandidacy(user_id, job_offer_id, action) {
        return axios.get(API_URL + "candidacies/accept?user_id=" + user_id + "&job_offer_id=" + job_offer_id + "&action=" + action);

    }
}

export default new CandidaciesService();
