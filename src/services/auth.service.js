import axios from "axios";

const API_URL = process.env.REACT_APP_BACKEND_URI;

class AuthService {
    login(username, password) {
        return axios
            .post(API_URL + "api/login_check", { username, password })
            .then((response) => {
                if (response.data.token) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem("user");
    }

    register(name, email, password, role) {
        return axios.post(API_URL + "register", {
            name,
            email,
            password,
            role
        });
    }
}

export default new AuthService();
