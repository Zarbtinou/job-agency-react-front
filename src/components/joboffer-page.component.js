import React, { Component } from "react";
import JobOfferService from "../services/joboffer.service";
import {connect} from "react-redux";
import { Modal } from 'react-responsive-modal';
import UserService from "../services/user.service";

class JobOfferPage extends Component {
    constructor(props) {
        super(props);
        this.handlePostulateOrFavorite = this.handlePostulateOrFavorite.bind(this);
        this.onFileChange = this.onFileChange.bind(this);

        this.state = {
            loading: true,
            jobOffer: "",
            user: "",
            filesList: "",
            open: false,
            successfullNewCandidacy: false,
            loadingNewCandidacy: false,
            errorNewCandidacy: false,
            message: ""
        }
    }

    componentDidMount() {
        this.setState({
            loading: true,
            user: JSON.parse(localStorage.getItem("user"))
        });
        JobOfferService.getOneJobOffer(this.props.match.params.id).then(
            response => {
                this.setState({
                    loading: false,
                    jobOffer: response.data
                });
            },
            error => {
                this.setState({
                    jobOffer:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString(),
                    loading: false
                });
            }
        )
    }

    handlePostulateOrFavorite(type){
        this.setState({
            loadingNewCandidacy: true
        })
        UserService.newCandidacyOrFavori(this.state.user.data.id, this.props.match.params.id, type, this.state.filesList).then(
            response => {
                this.setState({
                    content: response.data,
                    successfullNewCandidacy: true,
                    loadingNewCandidacy: false,
                });
                if (type === "candidacy") {
                    this.setState({
                        message: "Candidature envoyée !"
                    })
                } else if (type === "favori") {
                    this.setState({
                        message: "Offre ajoutée aux favoris !"
                    })
                }
                setTimeout(() => this.setState({open: false, successfullNewCandidacy: false}), 3000)
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString(),
                    errorNewCandidacy: true,
                    loadingNewCandidacy: false
                });
                setTimeout(() => this.setState({open: false, errorNewCandidacy: false}), 3000)
            }
        )
    }

    onFileChange(e) {
        const files = e.target.files;
        this.setState({
            filesList: files
        }, function () {
            console.log(this.state.filesList);
        })
    }

    render() {
        const loading = this.state.loading;
        const jobOffer = this.state.jobOffer;
        const { user: currentUser } = this.props;

        return(

            <div className="container">
                {!loading && (
                    <div className="row">
                        <h3 className="col-12">{jobOffer.title}</h3>
                        <span className="col-12">{jobOffer.company} - {jobOffer.city}</span>
                        <span className="col-12">Type de contrat : {jobOffer.contract_type} - {jobOffer.duration > 0 && "Durée : " + jobOffer.duration + " mois"}</span>
                        <span className="col-12">Estimation du salaire : {jobOffer.salary} € par mois</span>
                        <p className="col-12">{jobOffer.description}</p>
                        {currentUser.data.role === "Candidate" && (
                            <>
                                <button className="offset-2 col-3 btn btn-primary btn-block" onClick={() => this.setState({open: true})}>
                                    <span>Postuler</span>
                                </button>
                                <button className="offset-2 col-3 btn btn-primary btn-block" onClick={() => this.handlePostulateOrFavorite("favori")}>
                                    <span>Ajouter aux favoris</span>
                                </button>
                            </>
                        )}
                        {this.state.successfullNewCandidacy && (
                            <div className="alert alert-success offset-4 col-4" role="alert">
                                <span>{this.state.message}</span>
                            </div>
                        )}
                        {this.state.errorNewCandidacy && (
                            <div className="alert alert-danger offset-4 col-4" role="alert">
                                <span>Une erreur est survenue</span>
                            </div>
                        )}
                        <Modal open={this.state.open} onClose={() => this.setState({open: false})}>
                            <h3>Voulez vous joindre des fichiers ?</h3>
                            <input onChange={this.onFileChange} type="file" multiple/>
                            <button className="offset-4 col-4 btn btn-primary btn-block" onClick={ () => this.handlePostulateOrFavorite("candidacy")}>
                                <span>Postuler</span>
                            </button>

                            {this.state.successfullNewCandidacy && (
                                <div className="alert alert-success" role="alert">
                                    <span>{this.state.message}</span>
                                </div>
                            )}
                            {this.state.errorNewCandidacy && (
                                <div className="alert alert-danger" role="alert">
                                    <span>Une erreur est survenue</span>
                                </div>
                            )}
                        </Modal>
                    </div>

                )}
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { user } = state.auth;
    return {
        user,
    };
}

export default connect(mapStateToProps)(JobOfferPage);
