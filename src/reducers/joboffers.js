import {
    NEW_JOBOFFER_SUCCESS,
    NEW_JOBOFFER_FAIL
} from "../actions/types";

const initialState = {
    newJobOfferCreated: false
};
// eslint-disable-next-line
export default function (state = initialState, action) {
    const { type } = action;

    switch (type) {
        case NEW_JOBOFFER_SUCCESS:
            return {
                ...state,
                newJobOfferCreated: false
            };
        case NEW_JOBOFFER_FAIL:
            return {
                ...state,
                newJobOfferCreated: false
            };
        default:
            return state;
    }
}

