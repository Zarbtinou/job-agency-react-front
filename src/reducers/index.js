import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import joboffers from "./joboffers"

export default combineReducers({
  auth,
  message,
  joboffers
});
