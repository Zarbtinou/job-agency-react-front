import {
    NEW_JOBOFFER_SUCCESS,
    NEW_JOBOFFER_FAIL,
    SET_MESSAGE
} from "./types"

import JobOfferService from "../services/joboffer.service";

export const newJobOffer = (name, description, city, salary, owner_id, duration, contract_type, company, expiration_date) => (dispatch) => {
    return JobOfferService.newJobOffer(name, description, city, salary, owner_id, duration, contract_type, company, expiration_date).then(
        (response) => {
            dispatch({
                type: NEW_JOBOFFER_SUCCESS,
            });

            dispatch({
                type: SET_MESSAGE,
                payload: "Nouvel offre créer !"
            })

            return Promise.resolve();
        },
        (error) => {
            const message = "Erreur, veuillez verifier les champs"

            dispatch({
                type: NEW_JOBOFFER_FAIL,
            });

            dispatch({
                type: SET_MESSAGE,
                payload: message,
            });

            return Promise.reject();
        }
    )
}
