import React, { Component } from "react";
import UserService from "../services/user.service";
import JobOfferComponent from "./joboffer-component";

export default class MyCandidacies extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            loadingFav: true,
            user: "",
            candidacies: "",
            jobOffers: ""
        }
    }

    componentDidMount() {
        this.setState({
            loading: true,
            loadingFav: true,
            user: JSON.parse(localStorage.getItem("user"))
        }, function () {
            UserService.getAllCandidacies(this.state.user.data.id).then(
                response => {
                    this.setState({
                        candidacies: response.data,
                        loading: false
                    })
                },
                error => {
                    this.setState({
                        candidacies:
                            (error.response && error.response.data) ||
                            error.message ||
                            error.toString(),
                        loading: false
                    });
                }
            )
            UserService.getFavoris(this.state.user.data.id).then(
                response => {
                    this.setState({
                        jobOffers: response.data.job_offers,
                        loadingFav: false
                    });
                },
                error => {
                    this.setState({
                        jobOffers:
                            (error.response && error.response.data) ||
                            error.message ||
                            error.toString(),
                        loadingFav: false
                    });
                }
            )
        });
    }

    render() {
        const loading = this.state.loading;
        const loadingFav = this.state.loadingFav;
        const candidacies = this.state.candidacies;
        const jobOffers = this.state.jobOffers;

        return (
            <div className="container">
                <div className="row">
                    <h3 className="col-12">Vos offres favorites</h3>
                </div>
                { !loadingFav && (
                    <div className="row">
                        {jobOffers.map(jobOffer => (
                            <div key={jobOffer.title} className="col-12 col-lg-6">
                                <JobOfferComponent descLenght={240} jobOffer={jobOffer}/>
                            </div>
                        ))}
                    </div>
                )}
                <div className="row">
                    <h3 className="col-12">Mes candidatures</h3>
                </div>
                { !loading && (
                    <div className="row">
                        {candidacies.map(candidacy => (
                            <div key={candidacy.id} className="col-12 col-lg-6">
                                <div className="mainCard card">
                                    <h5 className="card-title mb-2 title">{candidacy.title}</h5>
                                    <h6 className="card-subtitle mb-2 company">{candidacy.company}</h6>
                                    <p className="card-text">{candidacy.status === "pending" ? "En attente" : (candidacy.status === "accepted") ? "Accepté ! Vous serez contacter prochainement" : (candidacy.status === "refused") ? "Refusé !" : "Inconnu"}</p>
                                </div>
                            </div>
                        ))}
                    </div>
                )}
            </div>
        )
    }
}
