import React, { Component } from "react";
import JobOfferComponent from "./joboffer-component";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import "../css/home.css"

import UserService from "../services/user.service";
import Input from "react-validation/build/input";
import Form from "react-validation/build/form";

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeCity = this.onChangeCity.bind(this);
        this.onChangeMax = this.onChangeMax.bind(this);

        this.state = {
            content: "",
            last10: "",
            loading: true,
            loadingResult: true,
            title: "",
            city: "",
            max: 25
        };
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value,
        }, function (){
            this.handleSearch();
        });

    }
    onChangeCity(e) {
        this.setState({
            city: e.target.value,
        }, function (){
            this.handleSearch();
        });
    }
    onChangeMax(e) {
        this.setState({
            max: e.target.value,
        }, function (){
            this.handleSearch();
        });
    }

    componentDidMount() {
        this.setState({
            loading: true
        });
        UserService.getJobOffers().then(
            response => {
                this.setState({
                    content: response.data,
                    last10: response.data.slice(0, 10),
                    loading: false,
                    loadingResult: false
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString(),
                    loading: false
                });
            }
        );
    }

    handleSearch() {
        UserService.getJobOffers(this.state.title, this.state.city, this.state.max, null).then(
            response => {
                this.setState({
                    content: response.data,
                    loadingResult: false
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString(),
                    loadingResult: false
                });
            }
        );

    }

    render() {
        const jobOffers = this.state.content;
        const last10jobOffers = this.state.last10;
        const loading = this.state.loading;
        const loadingResult = this.state.loadingResult;

        return (
            <div className="container">
                <div className="row">
                    {!loading && (
                        <div className="container col-12">
                            <Carousel showThumbs={false}>
                                {last10jobOffers.map(jobOffer => (
                                    <div key={jobOffer.title} className="maxWidthCarousel">
                                        <JobOfferComponent key={jobOffer.id} descLenght={155} jobOffer={jobOffer}/>
                                    </div>
                                ))}
                            </Carousel>

                        </div>
                    )}
                </div>
                {!loading && (
                    <div className="row">
                        <div className="col-12">
                            <h2>Rechercher une annonce</h2>
                        </div>
                        <Form
                            className="container row col-12"
                        >
                            <div className="col-3 form-group">
                                <label htmlFor="title">Nom du poste</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="title"
                                    value={this.state.title}
                                    onChange={this.onChangeTitle}
                                />
                            </div>
                            <div className="col-3 form-group">
                                <label htmlFor="city">Ville</label>
                                <Input
                                    type="text"
                                    className="form-control"
                                    name="title"
                                    value={this.state.city}
                                    onChange={this.onChangeCity}
                                />
                            </div>
                            <div className="col-3 form-group">
                                <label htmlFor="title">Nb de résultats</label>
                                <Input
                                    type="number"
                                    className="form-control"
                                    name="title"
                                    value={this.state.max}
                                    onChange={this.onChangeMax}
                                />
                            </div>
                        </Form>
                    </div>
                )}
                { !loadingResult && (
                    <div className="row">
                        {jobOffers.map(jobOffer => (
                            <div key={jobOffer.id} className="col-12 col-lg-6">
                                <JobOfferComponent key={jobOffer.name} descLenght={240} jobOffer={jobOffer}/>
                            </div>
                        ))}
                    </div>
                )}
            </div>
        );
    }
}
