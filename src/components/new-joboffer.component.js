import React, { Component } from "react";
import {connect} from "react-redux";
import {newJobOffer} from "../actions/joboffers";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Select from "react-validation/build/select";
import Textarea from "react-validation/build/textarea";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const required = (value) => {
  if (!value) {
    return (
        <div className="alert alert-danger" role="alert">
          Ce champs est requis !
        </div>
    );
  }
};


class NewJobOffer extends Component {
  constructor(props) {
    super(props);
    this.handleNewJobOffer = this.handleNewJobOffer.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeCity = this.onChangeCity.bind(this);
    this.onChangeSalary = this.onChangeSalary.bind(this);
    this.onChangeDuration = this.onChangeDuration.bind(this);
    this.onChangeContractType = this.onChangeContractType.bind(this);
    this.onChangeCompany = this.onChangeCompany.bind(this);
    this.onChangeExpirationDate = this.onChangeExpirationDate.bind(this);


    this.state = {
      name: "",
      description: "",
      city: "",
      salary: null,
      owner_id: this.props.user.data.id,
      duration: 0,
      contract_type: "",
      company: "",
      expiration_date: "",
      successful: false,
    };
  }

  onChangeName(e) {
    this.setState({
      name: e.target.value,
    });
  }
  onChangeDescription(e) {
    this.setState({
      description: e.target.value,
    });
  }
  onChangeCity(e) {
    this.setState({
      city: e.target.value,
    });
  }
  onChangeSalary(e) {
    this.setState({
      salary: e.target.value,
    });
  }
  onChangeDuration(e) {
    this.setState({
      duration: e.target.value,
    });
  }
  onChangeContractType(e) {
    this.setState({
      contract_type: e.target.value,
    });
  }
  onChangeCompany(e) {
    this.setState({
      company: e.target.value,
    });
  }
  onChangeExpirationDate(date) {
    this.setState({
      expiration_date: date,
    });
  }

  handleNewJobOffer(e) {
    e.preventDefault();

    this.setState({
      successful: false,
    });

    this.form.validateAll();

      this.props
          .dispatch(
              newJobOffer(this.state.name, this.state.description, this.state.city, this.state.salary, this.state.owner_id, this.state.duration, this.state.contract_type, this.state.company, this.state.expiration_date)
          )
          .then(() => {
            this.setState({
              successful: true
            });
          })
          .catch(() => {
            this.setState({
              successful: false,
            });
          });
  }

  render() {
    const { message } = this.props;
    return (
        <div className="container">
          <header className="jumbotron">
            <h3>Créer une nouvelle offre d'emploi</h3>
          </header>
          <div className="col-md-12">
            <Form
                className="container"
                onSubmit={this.handleNewJobOffer}
                ref={(c) => {
                  this.form = c;
                }}
            >
              {!this.state.successful && (
                  <div className="row">
                    <div className="col-12 form-group">
                      <label htmlFor="name">Titre</label>
                      <Input
                          type="text"
                          className="form-control"
                          name="name"
                          value={this.state.name}
                          onChange={this.onChangeName}
                          validations={[required]}
                      />
                    </div>
                    <div className="col-12 form-group">
                      <label htmlFor="description">Description</label>
                      <Textarea
                          type="text"
                          className="form-control"
                          name="description"
                          value={this.state.description}
                          onChange={this.onChangeDescription}
                          validations={[required]}
                      />
                    </div>
                    <div className="col-6 form-group">
                      <label htmlFor="company">Entreprise</label>
                      <Input
                          type="text"
                          className="form-control"
                          name="company"
                          value={this.state.company}
                          onChange={this.onChangeCompany}
                          validations={[required]}
                      />
                    </div>
                    <div className="col-6 form-group">
                      <label htmlFor="city">Ville</label>
                      <Input
                          type="text"
                          className="form-control"
                          name="city"
                          value={this.state.city}
                          onChange={this.onChangeCity}
                          validations={[required]}
                      />
                    </div>
                    <div className="col-3 form-group">
                      <label>Offre valable jusqu'au :</label>
                      <DatePicker selected={this.state.expiration_date} onChange={date => this.onChangeExpirationDate(date)} />
                    </div>
                    <div className="col-2 form-group">
                      <label>Type de contrat :</label>
                      <Select
                          name="contract_type"
                          value={this.state.contract_type}
                          onChange={this.onChangeContractType}
                      >
                        <option value='CDD'>CDD</option>
                        <option value='CDI'>CDI</option>
                      </Select>
                    </div>
                    <div className="col-3 form-group">
                      <label htmlFor="duration">Durée en mois du contrat</label>
                      <Input
                          type="number"
                          className="form-control"
                          name="duration"
                          value={this.state.duration}
                          onChange={this.onChangeDuration}
                          validations={[required]}
                      />
                    </div>
                    <div className="col-4 form-group">
                      <label htmlFor="salary">Estimation de salaire</label>
                      <Input
                          type="number"
                          className="form-control"
                          name="salary"
                          value={this.state.salary}
                          onChange={this.onChangeSalary}
                          validations={[required]}
                      />
                    </div>

                    <div className="col-12 form-group">
                      <button className="btn btn-primary btn-block">Créer une offre</button>
                    </div>
                  </div>
              )}

              {message && (
                  <div className="form-group">
                    <div className={ this.state.successful ? "alert alert-success" : "alert alert-danger" } role="alert">
                      {message}
                    </div>
                  </div>
              )}
            </Form>
          </div>
        </div>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  const { message } = state.message;
  return {
    user,
    message
  };
}

export default connect(mapStateToProps)(NewJobOffer);
