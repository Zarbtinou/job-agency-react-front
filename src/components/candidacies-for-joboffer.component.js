import React, { Component } from "react";
import JobOfferService from "../services/joboffer.service";
import CandidaciesService from "../services/candidacies.service";


export default class CandidaciesForJobOffer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            loadingAccept: false,
            jobOffer: "",
            candidacies: ""
        }
    }

    reloadData() {
        CandidaciesService.getCandidaciesForJobOffer(this.props.match.params.id).then(
            response => {
                this.setState({
                    loading: false,
                    candidacies: response.data
                });
            },
            error => {
                this.setState({
                    candidacies:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString(),
                    loading: false
                });
            }
        )
    }

    componentDidMount() {
        this.setState({
            loading: true,
        });
        JobOfferService.getOneJobOffer(this.props.match.params.id).then(
            response => {
                this.setState({
                    jobOffer: response.data
                }, this.reloadData);
            },
            error => {
                this.setState({
                    jobOffer:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString(),
                    loading: false
                });
            }
        )
    }

    handleAcceptCandidacy(user_id, job_offer_id, action) {
        this.setState({
            loadingAccept: true
        });
        CandidaciesService.handleAcceptCandidacy(user_id, job_offer_id, action).then(
            response => {
                this.setState({
                    loadingAccept: false
                }, function () {
                    JobOfferService.getOneJobOffer(this.props.match.params.id).then(
                        response => {
                            this.setState({
                                jobOffer: response.data
                            }, this.reloadData);
                        },
                        error => {
                            this.setState({
                                jobOffer:
                                    (error.response && error.response.data) ||
                                    error.message ||
                                    error.toString(),
                                loading: false
                            });
                        })
                });

            },
            error => {
                this.setState({
                    loadingAccept: false
                });
            }
        )
    }


    render() {
        const loading = this.state.loading;
        const jobOffer = this.state.jobOffer;
        const candidacies = this.state.candidacies;

        return(
            <div className="container">
                {!loading && (
                    <div className="row">
                        <h3 className="col-12">Candidatures pour l'offre {jobOffer.title} - {jobOffer.contract_type}</h3>
                        {candidacies.map(candidacy => (
                            <div key={candidacy.id} className="col-12 col-lg-6">
                                <div className="mainCard card">
                                    <h5 className="card-title mb-2 title">{candidacy.name}</h5>
                                    <h6 className="card-subtitle mb-2 company">{candidacy.username}</h6>
                                    <p className="card-text">{candidacy.status === "pending" ? "En attente" : (candidacy.status === "accepted") ? "Accepté ! Vous serez contacter prochainement" : (candidacy.status === "refused") ? "Refusé !" : "Inconnu"}</p>
                                    {candidacy.status === "pending" && (
                                        <>
                                            <button className="btn btn-primary btn-block" onClick={() => this.handleAcceptCandidacy(candidacy.user_id, this.props.match.params.id, "accept")}>Accepter</button>
                                            <button className="btn btn-danger btn-block" onClick={() => this.handleAcceptCandidacy(candidacy.user_id, this.props.match.params.id, "decline")}>Refuser</button>
                                        </>
                                    )}
                                </div>
                            </div>
                        ))}
                    </div>
                )}
            </div>
        )
    }
}
