import React, { Component } from "react";
import UserService from "../services/user.service";
import JobOfferComponent from "./joboffer-component";


export default class MyJobOffers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            content: ""
        }
    }

    componentDidMount() {
        this.setState({
            loading: true,
            user: JSON.parse(localStorage.getItem("user"))
        }, function (){
            UserService.getJobOffers("","",100, this.state.user.data.id).then(
                response => {
                    this.setState({
                        loading: false,
                        content: response.data
                    });
                },
                error => {
                    this.setState({
                        content:
                            (error.response && error.response.data) ||
                            error.message ||
                            error.toString(),
                        loading: false
                    });
                }
            )
        });
    }

    render() {
        const jobOffers = this.state.content;
        const loading = this.state.loading;

        return (
            <div className="container">
                <div className="row">
                    <h3 className="col-12">Vos offres d'emplois</h3>
                </div>
                { !loading && (
                    <div className="row">
                        {jobOffers.map(jobOffer => (
                            <div key={jobOffer.id} className="col-12 col-lg-6">
                                <JobOfferComponent key={jobOffer.name} descLenght={240} jobOffer={jobOffer}/>
                            </div>
                        ))}
                    </div>
                )}
            </div>
        )
    }
}
