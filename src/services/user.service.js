import axios from "axios";

const API_URL = process.env.REACT_APP_BACKEND_URI;

class UserService {
  getJobOffers(title, city, max, owner) {
    if (title || city || max ) {
      return axios.get(API_URL + "job_offers/search?title=" + title + "&city=" + city + "&limit=" + max + "&owner=" + owner)
    }
    else {
      return axios.get(API_URL + "job_offers/search");
    }
  }
  newCandidacyOrFavori(user_id, joboffer_id, type, fileList) {
    let route = "";
    if (type === "candidacy") {
      route = "candidacies/add";
    } else if (type === "favori") {
      route = "job_offers/add-favori";
    }
    return axios.post(API_URL + route, {
      user_id,
      joboffer_id,
      fileList
    })
  }
  getAllCandidacies(user_id) {
    return axios.get(API_URL + "candidacies?user_id=" + user_id)
  }
  getFavoris(user_id) {
    return axios.post(API_URL + "job_offers/favori", {user_id})
  }
}

export default new UserService();
