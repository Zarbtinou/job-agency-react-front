import axios from "axios";

const API_URL = process.env.REACT_APP_BACKEND_URI;

class JobOfferService {
    newJobOffer(name, description, city, salary, owner_id, duration, contract_type, company, expiration_date) {
        return axios.post(API_URL + "job_offers/new", {
            name,
            description,
            city,
            salary,
            owner_id,
            duration,
            contract_type,
            company,
            expiration_date
        });
    }
    getOneJobOffer(id) {
        return axios.get(API_URL + "job_offers/" + id);
    }

}

export default new JobOfferService();
